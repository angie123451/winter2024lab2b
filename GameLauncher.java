import java.util.Scanner;
public class GameLauncher{
	public static void main (String[]args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome");
		System.out.println("Which game would you like to play Hangman (1) or Wordle(2). Type the number");
		int game= reader.nextInt();
		/* if the user choice was hangman it will run the hangman game 
		*the choose wordle it will run wordle 
		but it choose neither, it will print that we dont have that game*/
		
		String replay = "yes";
		while(replay.equals("yes")){
			if(game==1){
				runhangman();
			}else if(game==2){
				runwordle();
			}else{
				System.out.println("Sorry we dont have "+ game +". Try something else");
			}
			// if the play want to replay the game, he can type yes to rerun the previous game he played
			System.out.println("Do you want to replay");
			replay = reader.next();
		}
	}
	
	public static void runhangman(){
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter a 4 letter word");
		//ask player a word
		String word=reader.next();
		//Convert to upperCase
		word = word.toUpperCase();
		//Start hangman game;
		Wordguesser.runGame(word);
		//return so that it can run the rest of the code (replay)
		return;
	}
	// it generate a wordle that the user will have to guess then run the game
	public static void runwordle(){
		final String answer = Wordle.generateanswer();
		Wordle.runGame(answer);
		//return so that it can run the rest of the code (replay)
		return;
	}
	
}