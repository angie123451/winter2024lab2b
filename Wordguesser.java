import java.util.Scanner;
public class Wordguesser{
	
	public static void runGame(String word){
		//start of the game
	System.out.println("Welcome to my game, Hangman!");
	Scanner reader = new Scanner(System.in);
	//if all boolean are false, it should not print any letter or the answer since the game didn't start yet
	boolean letter0=false;
	boolean letter1=false;
	boolean letter2=false;
	boolean letter3=false;
	//this is to say you have 6 life(try)
	int count= 0;
	//this to make sure when you guess all letter you dont need to guess again (stop)
	int right=0;
	//loop=> asking user to input a letter and print the result
		while(count<6&&right<4){
			System.out.println("Enter letter: ");
			String guesss=reader.next();
			// take the input and convert it into uppercase
			guesss=guesss.toUpperCase();
			char guess=guesss.charAt(0);
			int place=isLetterInWord(word,guess);
			if(place==0){
				letter0=true;
				right++;
			}else if(place==1){
				letter1=true;
				right++;
			}else if(place==2){
				letter2=true;
				right++;
			}else if (place==3){
				letter3=true;
				right++;
			}else {
				System.out.println("It doesn't have the letter "+guess);
			}
			printWord(word, letter0,letter1,letter2,letter3);
			count++;
		
		}
	// print if you won or lost
	if(right==4){
		//congrats!
		System.out.println("!!Congratulations!!!");
	}else{
		//show that you lost
		System.out.println("Game over!");
	}
	
	}
	//this method show what position if the guess or show -1 if the letter is not it the word
	public static int isLetterInWord(String word,char guess){
		int location=0;
		while(location < word.length()){
			if (word.charAt(location)==guess){
				// check if the letter you guessed is at the right position
				return location;
			}else if (word.charAt(location)!=guess&&location<3){
				location++;
			}else {
				// this is to mention later that the letter is not in the word
				return -1;
			}
		}
		return location;
	}
	
	public static void printWord(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3 ){
		
		System.out.print("Your result is ");
		// this is to only print 4 characters
		int where=0;
		/*print one character. either underscore or the letter in the word
		can't replace word.charAt(where)for a letter in the word
		because if the user change the word, it will */
		while(where<word.length()){
			//take boolean if it true and is the placement is correct, if it isnt print _  otherwise print the letter
			if (letter0==true&&where==0){
				System.out.print(word.charAt(where));
			}else if (letter1==true&&where==1){
				System.out.print(word.charAt(where));
			}else if(letter2==true&&where==2){
				System.out.print(word.charAt(where));
			}else if(letter3==true&&where==3){
				System.out.print(word.charAt(where));
			}else{
				System.out.print("_");
			}
			where++;
		}
		//this so that it doesn't print "Your result is _r__ enter a letter"
		//With this, it print "Your result is (next line) ar__" -next line- "enter letter: "
		System.out.println(" ");
		
	}
}