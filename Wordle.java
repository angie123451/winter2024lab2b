import java.util.Random;
import java.util.Scanner;
public class Wordle{
	//method choose a random answer
	public static String generateanswer(){
		String[] arrays= new String[]{"WHITE","JUDGE","EIGHT","YATCH","SLAVE","LIGHT","OLDER",
		"WASTE","PASTE","WHALE","TRASH","PIANO","NIGHT","LOVER","COVER","HOVER","PLANT","BREAK",
		"IMAGE","ZEBRA", "FIGHT", "BRAKE", "BREAD", "BLOCK", "SPRAY", "SHAME", "PLEAD", "DOVER", "LONER"};
		Random num = new Random();
		//random give a number between 0-28 inclu
		int nums = num.nextInt(28);
		String answer=arrays[nums];
		return answer;
	}
	//method check if the letter is in the answer
	public static boolean letterInanswer(String answer, char letter){
		
		for(int count=0; count< answer.length();count++ ){
			if (letter == answer.charAt(count)){
				return true;
			}
		}		
		return false;
	}
	//method check if the letter is in the answer and correct position
	public static boolean letterInSlot(String answer, char letter, int position){
		
		for (int place=0; place< answer.length(); place++){
			if (answer.charAt(place)== letter&& place== position){
				return true;
			}
		}
		return false;
	}
	//method create a array that depending on the letter is in the right spots/in the answer using the method already used
	public static String[] guessanswer(String answer, String guess){
		
		int length= answer.length();
		String[] colours = new String[length];
		for (int position=0; position<length;position++){
			char letter = guess.charAt(position);
			if(letterInSlot(answer,letter,position)== true){	
				colours[position]= "green";
			}else if (letterInanswer(answer, letter)== true){
				colours[position]= "yellow";
			}else{
				colours[position]= "white";
			}
		}
		return colours;
	}
	//this will print the letter according the color giving by guessanswer()
	public static void presentResults(String guess, String[] colours){
		
		final String GREEN= "\u001B[32m";
		final String YELLOW="\u001B[33m";
		final String WHITE= "\u001B[0m";
		for (int each=0;each<guess.length();each++){
			if (colours[each].equals("green")){
				System.out.print( GREEN + guess.charAt(each) + WHITE);
			}else if(colours[each].equals("yellow")){
				System.out.print( YELLOW + guess.charAt(each)  + WHITE);			
			}else{
				System.out.print( guess.charAt(each));
			}
		}
	}
	// ask the user to insert a answer and ask again if it not a 5 letter answer. 
	//also Capitlize the letter of their guess 
	public static String readGame(){
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter A Guess: ");
		String guess = reader.nextLine();
		while(guess.length()!=5){
			System.out.println("Enter A Guess Again: ");
			guess = reader.nextLine();
		}
		guess = guess.toUpperCase();
		return guess;
	}
	//run the game
	public static void runGame(String answer){
		System.out.println( "Welcome to Wordle! Your goal is to guess a 5-letter word.");
		for(int attempt=6; attempt>0; attempt--){
			System.out.println("You have "+ attempt +" attempts left");
			String guess = readGame();	
			String[] colours = guessanswer(answer, guess);
			presentResults(guess, colours);
			System.out.println(" ");
			if (guess.equals(answer)){
				System.out.println("YOU WIN!");
				//does that work? 
				return;
			}
		}
		System.out.println("You lost! TRY AGAIN");
		System.out.println("The answer was " + answer);
	}
}
